export const API = {
  url: 'https://api.zhuishushenqi.com',
  detailUrl: 'https://chapterup.zhuishushenqi.com',
  header: {
    "Content-Type": "application/json"
  },
  methodGet: 'GET'
};
