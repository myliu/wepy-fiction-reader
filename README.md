# wepy-one v1.0

## 小程序功能
- 书架
  - 显示订阅的书籍
- 分类
  - 大类列表
  - 小类列表
- 排行
- 小说阅读
  - 修改字体
  - 夜间模式
  - 左右切换章节
  - 换源
- 搜索
- 缓存
## 需要完善
* 目录和换源加个下划线或者字体变大或者间距变大
* 记录当前阅读进度
* UI调整,左滑删除
* 小说明细相关小说显示
* 书架下拉更新

## install
1. npm install wepy-cli -g
2. cd wepy-ficition-reader
3. npm install
4. wepy build --watch


#### 开发使用说明
1. 使用`微信开发者工具`新建项目，本地开发选择`dist`目录。
2. `微信开发者工具` --> 项目 --> 关闭ES6转ES5。
3. 本地项目根目录运行`wepy build --watch`，开启实时编译。

### 代码规范：
1. 变量与方法使用尽量使用驼峰式命名，避免使用`$`开头。
以`$`开头的方法或者属性为框架内建方法或者属性，可以被使用，使用前请[参考API文档](https://wepyjs.github.io/wepy/#/#api)。
2. 入口，页面，组件的命名后缀为`.wpy`。外链的文件可以是其它后缀。
请参考[wpy文件说明](https://wepyjs.github.io/wepy/#/#wpy文件说明)
3. 使用ES6语法开发。
框架在ES6下开发，因此也需要使用ES6开发小程序，ES6中有大量的语法糖可以让我们的代码更加简洁高效。
4. 使用Promise
框架默认对小程序提供的API全都进行了 Promise 处理，甚至可以直接使用`async/await`等新特性进行开发。

### 小程序开发文档
* https://mp.weixin.qq.com/debug/wxadoc/dev/

### wepy开发文档
* https://tencent.github.io/wepy/

### 感谢
本程序参考了 https://github.com/zimplexing/vue-nReader 的api文档


